FROM python:3.6

ADD ./app/requirements.txt /app/requirements.txt
ADD ./app/ /app/

WORKDIR /app/

RUN echo "testi"

RUN cd /app && pip install -r requirements.txt
RUN chmod -R 777 /app/run.sh

ENV GOOGLE_APPLICATION_CREDENTIALS=/app/credentials/firestore-eua-credential.json

ENTRYPOINT /app/run.sh
