# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from google.cloud import firestore
import time, datetime
from pprint import pprint

@shared_task(name='amil.tasks.exist')
def exist(datas):
	pprint(datas)
	pprint(datas[u'cellphone_number'])
	db = firestore.Client()
	doc_ref = db.collection(u'amil_users')
	try:
		user_ref = doc_ref.where(u'cellphone_number',u'==', datas[u'cellphone_number']).where(u'cellphone_area',u'==', datas[u'cellphone_area']).get()
		doc_found = [i.id for i in user_ref]
		yeah = {}
		yeah[u'total']=len(doc_found)
		yeah[u'keys']=doc_found
		return yeah
	except:
		return {'total':0,'keys':{}}

@shared_task(name='amil.tasks.add_user')
def add_user(datas):
	doc_exists = exist(datas)
	if(doc_exists[u'total']>0):
		return u'user already exists'

	try:
		db = firestore.Client()
		doc_ref = db.collection(u'amil_users')
		doc_ref.add(datas)
		return u'user added'
	except:
		return u'-error:not saved'

@shared_task(name='amil.tasks.suspend_user')
def suspend_user(datas):
	doc_exists = exist(datas)
	if(doc_exists[u'total']==0):
		return u'user not exists'

	try:
		db = firestore.Client()
		doc_ref = db.collection(u'amil_users')

		for doc_id in doc_exists[u'keys']:
			doc_ref.document(doc_id).update({
				'activated':0,
				'updated_at':datetime.datetime.utcnow(),
				'deleted_at':None
			})
		return u'user has been inatived'

	except AssertionError:
		return u'Error - not suspended'

@shared_task(name='amil.tasks.delete_user')
def delete_user(datas):
	doc_exists = exist(datas)
	if(doc_exists[u'total']==0):
		return u'user not exists'

	try:
		db = firestore.Client()
		doc_ref = db.collection(u'amil_users')
		for doc_id in doc_exists[u'keys']:
			doc_ref.document(doc_id).update({
				'activated':0,
				'updated_at':datetime.datetime.utcnow(),
				'deleted_at':datetime.datetime.utcnow()
			})
		return u'user has been deleted'

	except AssertionError:
		return u'Error - Not deleted'